﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public Vector3 Offset;
    public float Zoom;
    public float ZoomSpeed = 1.0f;

    public float CurrentZoom = 10f;
    private float _minZoom = 5f;
    private float _maxZoom = 15f;

    private Camera _camera;

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    private void Update()
    {
        HandleZoom();
    }

    void HandleZoom()
    {
        var zoomDelta = Input.GetAxis("Zoom") * ZoomSpeed;
        if (zoomDelta != 0)
        {
            CurrentZoom -= zoomDelta;

            if (CurrentZoom < _minZoom)
            {
                CurrentZoom = _minZoom;
            }
            else if (CurrentZoom > _maxZoom)
            {
                CurrentZoom = _maxZoom;
            }

            _camera.orthographicSize = CurrentZoom;
        }

    }

    void LateUpdate()
    {
        transform.position = Target.position + Offset;
    }

    public void SetTarget(Transform target)
    {
        this.Target = target;
    }
}
