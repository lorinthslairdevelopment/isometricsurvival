﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class DropTable
    {
        public DropItem[] Drops;
        private Random random = new Random();

        public DropItem GetNextDrop()
        {
            double roll = random.NextDouble() * 100;
            double current = 0d;

            foreach(var dropItem in Drops)
            {
                current += dropItem.Chance;

                if(current > roll)
                {
                    return dropItem;
                }
            }

            return null;
        }
    }
}
