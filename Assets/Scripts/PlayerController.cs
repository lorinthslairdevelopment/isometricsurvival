﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public LayerMask GroundMask;
    public LayerMask ResourceMask;
    public float MoveSpeed = 7.5f;
    public float SprintModifier = 1.5f;
    public float JumpStrength = 5f;

    private bool _onGround = false;
    private bool _isSprinting = false;
    private Vector3 _forward;
    private Vector3 _right;
    private Vector3 _direction;
    private Rigidbody _rigidbody;

    private void Start()
    {
        Transform t = Camera.main.transform;
        _forward = t.forward;
        _forward.y = 0f;
        _forward.Normalize();
        _right = t.right;
        _right.y = 0f;
        _right.Normalize();

        _rigidbody = GetComponent<Rigidbody>();
    }

    public Vector3 GetDirection()
    {
        return _direction;
    }

    public bool isOnGround()
    {
        return _onGround;
    }

    public bool isSprinting()
    {
        return _isSprinting;
    }

    // Update is called once per frame
    private void Update()
    {
        DetectGround();
        Move();
        Jump();
        Click();
    }

    private void DetectGround()
    {
        if(Physics.Raycast(transform.position, Vector3.down, 0.1f, GroundMask))
        {
            _onGround = true;
        }
        else
        {
            _onGround = false;
        }
    }

    private void Move()
    {
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            float changeH = Input.GetAxis("Horizontal");
            float changeV = Input.GetAxis("Vertical");

            UpdateDirection(changeH, changeV);

            float sprintSpeed = Input.GetButton("Sprint") ? SprintModifier : 1f;

            _rigidbody.MovePosition(transform.position + (_direction * MoveSpeed * sprintSpeed * Time.deltaTime));
            transform.LookAt(transform.position + _direction);
        }
    }

    private void Jump()
    {
        if (_onGround && Input.GetButton("Jump"))
        {
            _rigidbody.velocity = Vector3.up * JumpStrength;
        }
    }

    private void Click()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit, 100, ResourceMask))
            {
                GameObject hitResource = hit.collider.gameObject;
                Resource resource = hitResource.GetComponent<Resource>();
                resource.Damage(5.0f);
            }
        }
    }

    private void UpdateDirection(float h, float v)
    {
        Vector3 horizontal = new Vector3(_right.x * h, 0, _right.z * h);
        Vector3 vertical = new Vector3(_forward.x * v, 0, _forward.z * v);
        _direction = horizontal + vertical;
        _direction.Normalize();
    }

}