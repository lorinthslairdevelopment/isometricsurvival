﻿using Assets.Scripts.Enum;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    public float Health = 10;
    public DropTable[] DropTables;
    public DropShape DropShape;
    public Texture HealthFront;
    public Texture HealthBack;

    private float _maxHealth;
    private Vector2 _size = new Vector2(30, 5);

    private void Start()
    {
        _maxHealth = Health;
    }

    public void Damage(float damage)
    {
        Health -= damage;
        if(Health <= 0f)
        {
            Break();
        }
    }

    public void OnGUI()
    {
        if(Health != _maxHealth)
        {
            float percent = Health / _maxHealth;
            Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
            screenPos.y = Screen.height - screenPos.y;
            GUI.DrawTexture(new Rect(screenPos.x - (_size.x / 2), screenPos.y + (_size.y / 2), _size.x, _size.y), HealthBack);
            GUI.DrawTexture(new Rect(screenPos.x - (_size.x / 2), screenPos.y + (_size.y / 2), _size.x * percent, _size.y), HealthFront);
        }
    }

    private void Break()
    {
        DropItems();
        Destroy(gameObject);
    }

    private DropItem[] GetDroppedItems()
    {
        List<DropItem> drops = new List<DropItem>();
        foreach(var table in DropTables)
        {
            DropItem dropItem = table.GetNextDrop();
            if(dropItem != null)
            {
                drops.Add(dropItem);
            }
        }
        return drops.ToArray();
    }

    private void DropItems()
    {
        DropItem[] drops = GetDroppedItems();
        if (drops.Length > 0)
        {
            switch (DropShape)
            {
                case DropShape.Line:
                    BreakLine();
                    break;
                case DropShape.Circle:
                    BreakCircle();
                    break;
            }
        }
    }

    private void BreakLine()
    {
        //Drop resources in a line
    }

    private void BreakCircle()
    {
        //Drop resources in a circle
    }
}
